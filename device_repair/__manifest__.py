# -*- coding: utf-8 -*-
{
    'name': "device_repair",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','website', 'account', 'website_form'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'security/device_repair_security.xml',
        'report/service_report.xml',
        'data/sequence_service_guide.xml',
        'data/mails/notification_service.xml',
        'templates/assets_frontend.xml',
        'views/service_guide.xml',
        'views/service_guide_report.xml',
        'templates/service_guide_t.xml',
        'templates/service_guide_record.xml',

        
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}