# -*- coding: utf-8 -*-

from odoo import models, fields, api
import base64


class serviceGuides(models.Model):
    _name = 'dr.service.guide'
    _rec_name = 'code'
    _description = 'module to keep track of the process of electronic device repair'

    def compute_discount(self, price, discount):
        if discount:
            total_descount = price * (discount / 100.0)
            return price - total_descount
        else:
            return price
    @api.one
    @api.depends('product_ids.price')
    def _compute_amount(self):
        amount = sum(self.compute_discount(line.price,line.discount) for line in self.product_ids)
        self.amount_total = amount

    user_id = fields.Many2one('res.users', string='Owner', default=lambda self: self.env.uid, help="Owner of the note stage")
    code = fields.Char(string="code")
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('approved', 'Aprovada'),
        ('complete', 'Completa'),
        ('cancel', 'Cancelada'),
        ('finished', 'Finalizada')
    ],default='draft')
    partner_id = fields.Many2one('res.partner', string="Partner")
    observations = fields.Text(string='observations')
    amount_total = fields.Monetary(string="amount", store=True, readonly=True, description="amount to be paid", compute='_compute_amount')
    product_ids = fields.One2many('dr.service.guide.product','service_guide_id', string='Product')
    currency_id = fields.Many2one('res.currency', string='Currency',
        required=True, readonly=True, states={'draft': [('readonly', False)]},
        default=lambda self: self.env.user.company_id.currency_id, track_visibility='always')
    

    def value_code(self, vals):
        vals.setdefault('code', self.env['ir.sequence'].next_by_code('dr.service.guide'))

    @api.model
    def create(self,vals):
        self.value_code(vals)
        record = super(serviceGuides,self).create(vals)
        return record

    def service_approved(self):
        self.state = 'approved'

    def service_complete(self):
        self.state = 'complete'

    def service_cancel(self):
        self.state = 'cancel'

    def service_finished(self):
        self.state = 'finished'

    def send_pdf(self):
        if self._context.get('pdf'):
            print("send_pdfffffffffffffffffffff")
            
            
            template_obj = self.sudo().env.ref('device_repair.email_client_servicwwwqa')
            if template_obj:
                template_obj.send_mail(self.id, force_send=True, raise_exception=True)

class ServiceGuideProduct(models.Model):
    _name = 'dr.service.guide.product'
    _description = 'Module where the arrangements to be repaired will be registered'

    price = fields.Float(string="Price", related='product_id.lst_price', store=True, depends=['product_id'])
    discount = fields.Float(string="Discount")
    product_id = fields.Many2one('product.product', string="Name")
    service_guide_id = fields.Many2one('dr.service.guide', string="service guide")
