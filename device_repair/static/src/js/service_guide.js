odoo.define("device_repair.service_guide", function (require) {
    "use strict";
  
    var ajax = require("web.ajax");
    var utils = require("web.utils");
    var core = require("web.core");
    var Dialog = require("web.Dialog");
    var session = require("web.session");
    var sAnimations = require("website.content.snippets.animation");

  
    var _t = core._t;
    var qweb = core.qweb;
  
    sAnimations.registry.serviceGuideTree = sAnimations.Class.extend({
      selector: "#TreeService",
  
      read_events: {
        'click tr[id*="AIRP_"]': "_onClickSearchservice",
      },
  
      start: function () {
        var self = this;
        $(document).ready(function() {
          $("#tableService").DataTable();
        });
        
      },
  
      _onClickSearchservice: function (ev) {
        var tr = $(ev.target).parent();
        var datos = tr.attr("id").split("_");
        var redirect =
          "/service/record/" + datos[1];
        $(window.location).attr("href", redirect);
      },
  
    });
});
  