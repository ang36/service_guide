# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
from odoo.addons.portal.controllers.web import Home
import logging
import json

_logger = logging.getLogger(__name__)

class Home(Home):

    @http.route('/', type='http', auth="public", website=True)
    def home(self, *args, **kw):
        service = request.env['dr.service.guide'].sudo().search([])
        values = {
            'services': service
        }

        return http.request.render('device_repair.service', values)


class ServiceGuide(http.Controller):

    @http.route('/service', auth="user", type='http', website=True)
    def view(self, **post):
        service = request.env['dr.service.guide'].sudo().search([])
        values = {
            'services': service
        }
        return http.request.render('device_repair.service', values)

    @http.route('/service/record/<model("dr.service.guide"):record>', auth="user", website=True, csrf=True)
    def serviceRecord(self, record, **post):
        values = {
            'record': record
        }
        return http.request.render('device_repair.serviceRecord', values)

    
    @http.route('/service/write/<model("dr.service.guide"):record>', methods=['POST'], auth="user", website=True)
    def writeService(self, record, **post):
        observations = post['observations']
        vals = {}
        if record.observations != observations:
            vals.update({'observations': observations})
        if vals:
            record.write(vals)
        services = request.env['dr.service.guide'].sudo().search([])
        service_data = {
            'services': services
        }
        return http.request.render('device_repair.service', service_data)